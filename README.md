# Django Blog Example

This repository follows through Chapters 5 to 7 of Django for Beginners (2.1) by William S. Vincent.

The mentioned chapters focused on creating a blog app, with support for Django forms, login, logout, and sign-up functionality.
